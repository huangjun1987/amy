﻿/*
1. **iqTest** Bob is preparing to pass an IQ test. The most frequent task in this test 
    is to find out which one of the given numbers differs from the others. Bob observed
    that one number usually differs from the others in evenness. Help Bob — to check his 
    answers, he needs a program that among the given numbers finds one that is different in 
    evenness, and return the position of this number. _Keep in mind that your task is to help 
    Bob solve a real IQ test, which means indexes of the elements start from 1 (not 0)_

		iqTest("2 4 7 8 10") → 3 //third number is odd, while the rest are even
		iqTest("1 2 1 1") → 2 // second number is even, while the rest are odd
		iqTest("") → 0 // there are no numbers in the given set
        iqTest("2 2 4 6") → 0 // all numbers are even, therefore there is no position of an odd number
*/
function iqTest(num){
    let result = 0;
    let isEven = false;
    let cntEven = 0;
    let cntOdd = 0;
    let latestEvenIndex = 0;
    let latestOddIndex = 0;
    let numArray = num.trim().split(" ");
    console.log(numArray);
    if (numArray.length == 0) {
        return 0;
    }
   for(let i=0; i< numArray.length; i++){
       isEven = numArray[i]%2==0;
       console.log('numArray[i]' + numArray[i]);
       console.log('isEven' + isEven);
       if(isEven){
           cntEven++;
           latestEvenIndex = 1;
           if(cntOdd>1)
           {
               return i + 1;
           }
           if(latestOddIndex>0&& cntEven>1){
               return latestOddIndex+1;
           }
       }else{
           cntOdd++;
           latestOddIndex=i;
           if(cntEven>1){
               return i + 1;
           }
           if(latestEvenIndex>0&& cntOdd>1){
            return latestEvenIndex+1;
        }
       }
   }
   return 0;
}

/*
2. **titleCase** Write a function that will convert a string into title case, given an optional 
    list of exceptions (minor words). The list of minor words will be given as a string with each 
    word separated by a space. Your function should ignore the case of the minor words string -- 
    it should behave in the same way even if the case of the minor word string is changed.


* First argument (required): the original string to be converted.
* Second argument (optional): space-delimited list of minor words that must always be lowercase
except for the first word in the string. The JavaScript tests will pass undefined when this 
argument is unused.

		titleCase('a clash of KINGS', 'a an the of') // should return: 'A Clash of Kings'
		titleCase('THE WIND IN THE WILLOWS', 'The In') // should return: 'The Wind in the Willows'
        titleCase('the quick brown fox') // should return: 'The Quick Brown Fox'
*/
function titleWord(word) {
    let nWord = '';
    let uLetter = '';
    let tWord = '';

    nWord = word.toLowerCase();
    nWord = word.substr(1,word.length);
    uLetter = word.substr(0,1).toUpperCase();
    tWord = uLetter + nWord;
    return tWord;
}
function titleCase(title, minorWords) {

    let newTitleCase = '';
    let holdArrayTitle = [];
    let holdTitle = title;
    let holdMinor = minorWords;
    let holdArrayMinorWord = [];
    
    holdArrayTitle = title.trim().toLowerCase().split(" ");
    holdMinor = minorWords.trim().toLowerCase().split(" ");
    
    for (let i=0; i < holdArrayTitle.length; i++){
        if (holdMinor.length != 0) {
            for (let j=0; j < holdMinor.length; j++) {
                if (holdArrayTitle[i] == holdMinor[j] && i != 0) {  //checks to see if word is in minor word list or not first word
                    newTitleCase = holdArrayTitle[i].toLowerCase();
                }
                else if (holdArrayTitle[i] == holdMinor[j]) {  //convert word to titleCase
                    newTitleCase = titleWord(holdArrayTitle[i]);
                }
            }  //end of the j-for loop (minor words)
        }
        else {
            newTitleCase = titleWord(holdArrayTitle[i]);
        }  
        if (i != holdArrayTitle.length) {
            newTitleCase = newTitleCase + ' ' ;
        }

    } //end of the i-for loop (title)
    
           
              
      
}