let problemNumber = 1;
let currentScore = 0;
let correctAnswer = 0;
let operators = [];
/**
 * Utility function to generate a random number based on max
 * @param {number} max
 */
function getRandomNumber(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  /**
 * Utility function to shuffle the items in an array
 * @param {object} arr
 */
function shuffleArray(arr) {
    return arr.sort(function (a, b) { return Math.random() - 0.5 })
  }

function updateProblemNumberDisplay() {
    const displayProblemNum = document.querySelector('.currentProblem');
    displayProblemNum.innerText = problemNumber;
}

function updateScoreDisplay() {
    const displayScore = document.querySelector('.currentScore');
    displayScore.innerText = currentScore;
}

function generateOperatorSelection(){
    hideMathSection();
}
function hideSelectOperatorsSection(){
    let elements = document.getElementsByClassName('select-operators');
    for (let element of elements){
        element.style.display = 'none';     
    }
}
function showSelectOperatorsSection(){

    let operatorCheckboxes = document.getElementsByClassName('operator')
    for (let operatorCheckbox of operatorCheckboxes){
        operatorCheckbox.checked = false;
    }   

    let elements = document.getElementsByClassName('select-operators');
    for (let element of elements){
        element.style.display = 'block';     
    }
}
function hideMathSection(){
    let elements = document.getElementsByClassName('math');
    for (let element of elements){
        element.style.display = 'none';     
    }
}
function showMathSection(){
    let elements = document.getElementsByClassName('math');
    for (let element of elements){
        element.style.display = 'block';     
    }
}
function getRandomOperator(){
    let randomIndex = Math.floor(Math.random() * operators.length);
    return operators[randomIndex];
}
function generateQuestionAndAnswers() {
    const operator = getRandomOperator();
    
    let numOne = getRandomNumber(10);
    let numTwo = getRandomNumber(10);
    

    let problem = document.querySelector('.expression');
    console.log(operator);
    if(operator=='+'){
        problem.innerText = numOne + ' ' + operator + ' ' + numTwo;
        correctAnswer = numOne + numTwo;
    }
    else if(operator=='-'){
        if(numOne >= numTwo)
        {
            problem.innerText = numOne + ' ' + operator + ' ' + numTwo;
            correctAnswer = numOne - numTwo;
        }else
        {
            problem.innerText = numTwo + ' ' + operator + ' ' + numOne;
            correctAnswer = numTwo - numOne;
        }       
    }else if(operator=='*'){
        problem.innerText = numOne + ' ' + operator + ' ' + numTwo;
        correctAnswer = numOne * numTwo;
    }
    else if(operator=='/'){
        while(numOne == 0){ // make sure numOne is not 1 so it can be used as divider in division
            numOne = getRandomNumber(10);
        }
        problem.innerText = numOne * numTwo + ' ' + operator + ' ' + numOne;
        correctAnswer = numTwo;
    }
    let answers = [];
    answers.push(correctAnswer);

    while (answers.length < 4)  //checks if a number is a duplicate number in the array
    {
        let randomNumber = getRandomNumber(82);
        if (!answers.includes(randomNumber))
        {
            answers.push(randomNumber);
            
        }
    }

    answers = shuffleArray(answers);

    populateAnswers(answers);
}
function populateAnswers(answers){
    let answersList = document.querySelectorAll('li');
    answersList.forEach((li, index) => {
        li.innerText=answers[index];
    });
}
function handleAnswer(event) {
    let answer = event.target.innerText;
    if (answer == correctAnswer)
    {
        currentScore++;
        updateScoreDisplay();
        updateProblemNumberDisplay();
        problemNumber++;
    }
    else 
    {
        updateProblemNumberDisplay();
        problemNumber++;
    }

    if (problemNumber <= 10) {
        generateQuestionAndAnswers();
    }
    else
    {
        const listItems = document.querySelectorAll('li');
        listItems.forEach((item) => {
            item.removeEventListener('click', handleAnswer);
    })
        hideQASection();
    }   
}


function hideQASection(){
    let elements = document.getElementsByClassName('show-hide');
    for (let element of elements){
        element.style.display = 'none';
       
    }
}

function showQASection(){
    let elements = document.getElementsByClassName('show-hide');
    for (let element of elements){
        element.style.display = 'block';
       
    }
}
function start(event)
{
    selectOperators();
    hideSelectOperatorsSection();
    showMathSection();
    generateQuestionAndAnswers();
}
function selectOperators(){
    let elements = document.getElementsByClassName('operator')
    for (let element of elements){
        if(element.checked){
            operators.push(element.value);
        }
    }   
}
function startOver(event)
{
    problemNumber = 1;
    currentScore = 0;
    correctAnswer = 0;
    updateProblemNumberDisplay();
    updateScoreDisplay();
    generateQuestionAndAnswers();
    const listItems = document.querySelectorAll('li');
    listItems.forEach((item) => {
        item.addEventListener('click', handleAnswer);
    })
    showQASection();
    showSelectOperatorsSection();
    hideMathSection();
}

  document.addEventListener('DOMContentLoaded',()=>{      
            showOperatorsSelection();
            const listItems = document.querySelectorAll('li');
            listItems.forEach((item) => {
            item.addEventListener('click', handleAnswer);
            const startOverBtn = document.getElementById('btnStartOver');
            startOverBtn.addEventListener('click', startOver);

            const startBtn = document.getElementById('btnStart');
            startBtn.addEventListener('click', start);
        })
}
)