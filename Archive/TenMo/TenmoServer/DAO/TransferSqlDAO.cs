﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public class TransferSqlDAO : ITransferDAO
    {
        private string connectionString;

        private IAccountDAO Acct;

        public TransferSqlDAO(string databaseConnectionString)
        {
            connectionString = databaseConnectionString;
        }
        private Transfer GetTransferFromReader(SqlDataReader reader)
        {
            Transfer transfer = new Transfer()
            {
                Transfer_Id = Convert.ToInt32(reader["transfer_id"]),
                Transfer_Type_Id = Convert.ToInt32(reader["transfer_type_id"]),
                Transfer_Status_Id = Convert.ToInt32(reader["transfer_status_id"]),
                Account_From = Convert.ToInt32(reader["account_from"]),
                Account_To = Convert.ToInt32(reader["account_to"]),
                Amount = Convert.ToDecimal(reader["amount"]),
            };
            return transfer;
        }

        public bool CreateTransfer(Account fromAcct, Account toAcct, TransferDTO transferDTO)
        {
            bool success = false;
         
            if (fromAcct.Balance < transferDTO.Amount)
            {
                return false; //insufficient funds
            }

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    // first UPDATE stmt does the subtraction of balance - amt of the sender
                    // second UPDATE stmt does the addition of balance + amt of the reciver
                    string statement = @"INSERT INTO [dbo].[transfers]
           ([transfer_type_id]
           ,[transfer_status_id]
           ,[account_from]
           ,[account_to]
           ,[amount])
     VALUES
           (@transfer_type, @transfer_status, @user_id, @user_id_to, @amt) ";
                    if (transferDTO.TransferStatus == 2)
                    {
                        statement += @"UPDATE accounts SET balance = balance - @amt WHERE user_id = @user_id; 
                                       UPDATE accounts SET balance = balance + @amt WHERE user_id = @user_id_to; ";
                    }
                    SqlCommand cmd = new SqlCommand(statement, conn);
                    cmd.Parameters.AddWithValue("@user_id", fromAcct.User_Id);
                    cmd.Parameters.AddWithValue("@user_id_to", toAcct.User_Id);
                    cmd.Parameters.AddWithValue("@amt", transferDTO.Amount);
                    cmd.Parameters.AddWithValue("@transfer_type", transferDTO.TransferType);
                    cmd.Parameters.AddWithValue("@transfer_status", transferDTO.TransferStatus);

                    int numberOfRowsEffected = cmd.ExecuteNonQuery();


                    if (numberOfRowsEffected > 0)
                    {

                        success = true;
                    }
                        

                }
            }
            catch (SqlException)
            {
                throw;
            }
            return success;
        }

        //if ok return status approved
        //else return status rejected



        public Transfer GetTransferById(int transfer_id)
        {
            Transfer returnTransfer = new Transfer();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"SELECT t.transfer_id, tt.transfer_type_desc, 
ts.transfer_status_desc , t.account_from, t.account_to, t.amount 
FROM transfers t
join transfer_types tt on t.transfer_type_id = tt.transfer_type_id
join transfer_statuses ts on t.transfer_status_id = ts.transfer_status_id
                                                    WHERE t.transfer_id = @transfer_id", conn);
                    cmd.Parameters.AddWithValue("@transfer_id", transfer_id);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        returnTransfer.Transfer_Id = Convert.ToInt32(reader["transfer_id"]);
                        returnTransfer.transfer_type_desc = reader["transfer_type_desc"].ToString();
                        returnTransfer.transfer_status_desc = reader["transfer_status_desc"].ToString();
                        returnTransfer.Account_From = Convert.ToInt32(reader["account_from"]);
                        returnTransfer.Account_To = Convert.ToInt32(reader["account_to"]);
                        returnTransfer.Amount = Convert.ToDecimal(reader["amount"]);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return returnTransfer;
        }

        public List<Transfer> GetTransferList(int userID)
        {
            List<Transfer> transferList = new List<Transfer>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(@"SELECT transfer_id, 
                                                    account_from, account_to, amount
                                                    FROM transfers 
                                                    WHERE account_from = @user_id or account_to = @user_id", conn);
                    cmd.Parameters.AddWithValue("@user_id", userID);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Transfer transfer = new Transfer();

                        transfer.Transfer_Id = Convert.ToInt32(reader["transfer_id"]);
                        transfer.Account_From = Convert.ToInt32(reader["account_from"]);
                        transfer.Account_To = Convert.ToInt32(reader["account_to"]);
                        transfer.Amount = Convert.ToDecimal(reader["amount"]);
                        transferList.Add(transfer);
                    }
                }
            }
            catch (SqlException)
            {
                throw;
            }

            return transferList;
        }

    }
}

