﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TenmoClient.Data
{
    public class API_Transfer_Detail
    {
        public int transfer_Id { get; set; }
        public string transfer_type_desc { get; set; }
        public string transfer_status_desc { get; set; }
        public int account_From { get; set; }
        public int account_To { get; set; }
        public decimal amount { get; set; }

    }
}
