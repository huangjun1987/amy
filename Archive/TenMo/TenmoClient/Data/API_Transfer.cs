﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TenmoClient.Data
{
    public class API_Transfer
    {
        public int From_User_Id { get; set; }
        public int To_User_Id { get; set; }
        public decimal Amount { get; set; }
        public int TransferType { get; set; }
        public int TransferStatus { get; set; }


    }
}
