﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using TenmoClient.Data;
using TenmoServer.Models;

namespace TenmoClient
{
    public class TransferService
    {
        private readonly static string API_BASE_URL = "https://localhost:44315/";
        private readonly IRestClient client = new RestClient();

        public void CreateTransfer(API_Transfer transfer)
        {
            RestRequest request = new RestRequest(API_BASE_URL + "transfer/transfer");
            request.AddJsonBody(transfer);
            IRestResponse<bool> response = client.Post<bool>(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                Console.WriteLine("An error occurred communicating with the server.");
            }
            else if (!response.IsSuccessful)
            {
                Console.WriteLine("An error message was received during transfer");
            }
            else if(response.Data)
            {
                Console.WriteLine("Transfer was successful!");
            }
        }
        public List<API_Transfer_Detail> GetTranferList(int userId)
        {
            RestRequest request = new RestRequest(API_BASE_URL + "transfer/transfers/" + userId);
            IRestResponse<List<API_Transfer_Detail>> response = client.Get<List<API_Transfer_Detail>>(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                Console.WriteLine("An error occurred communicating with the server.");
                return null;
            }
            else if (!response.IsSuccessful)
            {
                Console.WriteLine("An error message was received during transfer");
                return null;
            }
            else
            {
                return response.Data;
            }


        }

        internal API_Transfer_Detail GetTranferDetail(int transferId)
        {
            RestRequest request = new RestRequest(API_BASE_URL + "transfer/transfer/" + transferId);
            IRestResponse<API_Transfer_Detail> response = client.Get<API_Transfer_Detail>(request);

            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                Console.WriteLine("An error occurred communicating with the server.");
                return null;
            }
            else if (!response.IsSuccessful)
            {
                Console.WriteLine("An error message was received during transfer");
                return null;
            }
            else
            {
                return response.Data;
            }
        }
    }
}
