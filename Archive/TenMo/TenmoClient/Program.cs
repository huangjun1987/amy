﻿using System;
using System.Collections.Generic;
using TenmoClient.Data;
using System.Web;
using System.Net;
using RestSharp;
using System.IO;
using System.Net.Http;
using System.Linq;

namespace TenmoClient
{
    class Program
    {
        private static readonly ConsoleService consoleService = new ConsoleService();
        private static readonly AuthService authService = new AuthService();
        private static readonly TransferService transferService = new TransferService();

        static void Main(string[] args)
        {
            Run();
        }
        private static void Run()
        {
            int loginRegister = -1;
            while (loginRegister != 1 && loginRegister != 2)
            {
                Console.WriteLine("Welcome to TEnmo!");
                Console.WriteLine("1: Login");
                Console.WriteLine("2: Register");
                Console.Write("Please choose an option: ");

                if (!int.TryParse(Console.ReadLine(), out loginRegister))
                {
                    Console.WriteLine("Invalid input. Please enter only a number.");
                }
                else if (loginRegister == 1)
                {
                    while (!UserService.IsLoggedIn()) //will keep looping until user is logged in
                    {
                        LoginUser loginUser = consoleService.PromptForLogin();
                        API_User user = authService.Login(loginUser);
                        if (user != null)
                        {
                            UserService.SetLogin(user);
                        }
                    }
                }
                else if (loginRegister == 2)
                {
                    bool isRegistered = false;
                    while (!isRegistered) //will keep looping until user is registered
                    {
                        LoginUser registerUser = consoleService.PromptForLogin();
                        isRegistered = authService.Register(registerUser);
                        if (isRegistered)
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Registration successful. You can now log in.");
                            loginRegister = -1; //reset outer loop to allow choice for login
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Invalid selection.");
                }
            }

            MenuSelection();
        }
        
        private static void MenuSelection()
        {
            int menuSelection = -1;
            while (menuSelection != 0)
            {
                Console.WriteLine("");
                Console.WriteLine("Welcome to TEnmo! Please make a selection: ");
                Console.WriteLine("1: View your current balance");
                Console.WriteLine("2: View your past transfers");
                Console.WriteLine("3: View your pending requests");
                Console.WriteLine("4: Send TE bucks");
                Console.WriteLine("5: Request TE bucks");
                Console.WriteLine("6: Log in as different user");
                Console.WriteLine("7. View all transfers");
                Console.WriteLine("8. retrieve the details of any transfer based upon the transfer ID");
                Console.WriteLine("0: Exit");
                Console.WriteLine("---------");
                Console.Write("Please choose an option: ");

                if (!int.TryParse(Console.ReadLine(), out menuSelection))
                {
                    Console.WriteLine("Invalid input. Please enter only a number.");
                }
                else if (menuSelection == 1)
                {

                    int userId = UserService.GetUserId();  //GetUserId is contained in the UserService.cs
                    decimal balance = authService.GetBalance(userId);
                    Console.WriteLine("Your current account balance is: $" + balance);
                }
                else if (menuSelection == 2)
                {
                    // view past transfers
                    int pastTransfers = consoleService.PromptForTransferID("approve");
                }
                else if (menuSelection == 3)
                {
                    //view pending transfers
                    int pendingTransfers = consoleService.PromptForTransferID("view");
                }
                else if (menuSelection == 4)
                {
                    List<User> userList = ShowUserList();  //Gets list of users that money can be sent to

                    int userIdTo = GetUserId(userList, "Choose the Id of who to send money to:  ");  //Get the user to send TE Bucks to and catches errors for invalid entrys

                    int userId = UserService.GetUserId();  //GetUserId is contained in the UserService.cs
                    decimal balance = authService.GetBalance(userId);
                    decimal amount = GetAmount(balance, "Send");

                    API_Transfer transferDTO = new API_Transfer();
                    transferDTO.From_User_Id = userId;
                    transferDTO.To_User_Id = userIdTo;
                    transferDTO.Amount = amount;
                    transferDTO.TransferType = 2;
                    transferDTO.TransferStatus = 2;
                    bool result = transferService.CreateTransfer(transferDTO);

                

                    //get reciever

                    //transfer the money to reciever from sender

                    //adjust the balance of the reciever

                    //adjust the balance of the sender


                }
                else if (menuSelection == 5)
                {
                    List<User> userList = ShowUserList();  //Gets list of users that money can be requested

                    int userIdFrom = GetUserId(userList, "Choose the Id of who to send money to:  ");  //Get the user to send TE Bucks to and catches errors for invalid entrys

                    int userId = UserService.GetUserId();  //GetUserId is contained in the UserService.cs
                    decimal balance = authService.GetBalance(userId);
                    decimal amount = GetAmount(balance, "Request");

                    API_Transfer transferDTO = new API_Transfer();
                    transferDTO.From_User_Id = userIdFrom;
                    transferDTO.To_User_Id = userId;
                    transferDTO.Amount = amount;
                    transferDTO.TransferType = 1;
                    transferDTO.TransferStatus = 1;

                    bool result = transferService.CreateTransfer(transferDTO);

                }
                else if (menuSelection == 6)
                {
                    Console.WriteLine("");
                    UserService.SetLogin(new API_User()); //wipe out previous login info
                    Run(); //return to entry point
                }
                else if (menuSelection == 7)
                {
                    int userId = UserService.GetUserId();  //GetUserId is contained in the UserService.cs
                    List<API_Transfer_Detail> transferList = transferService.GetTranferList(userId);
                    if (transferList != null)
                    {
                        Console.WriteLine("ID\tFrom/To\tAmount");
                        Console.WriteLine("-------------------------------------------");
                        List<User> userList = authService.ListUsers();
                        foreach (var transfer in transferList)
                        {
                            if (transfer.account_From != userId)
                            {
                                string username = userList.Where(u => u.UserId == transfer.account_From).Select(u => u.Username).FirstOrDefault();
                                Console.WriteLine($"{transfer.transfer_Id}\tFrom:  {username}\t{transfer.amount.ToString("c2")}");
                            }
                            else
                            {
                                string username = userList.Where(u => u.UserId == transfer.account_To).Select(u => u.Username).FirstOrDefault();
                                Console.WriteLine($"{transfer.transfer_Id}\tTo:  {username}\t{transfer.amount.ToString("c2")}");
                            }

                        }
                    }
                }
                else if (menuSelection == 8)
                {
                    int userId = UserService.GetUserId();  //GetUserId is contained in the UserService.cs
                    List<API_Transfer_Detail> transferList = transferService.GetTranferList(userId);
                    if (transferList != null)
                    {
                        int transferId = GetTransferId(transferList);
                        API_Transfer_Detail transfer_Detail = transferService.GetTranferDetail(transferId);

                        Console.WriteLine("Transfer Details");
                        Console.WriteLine("-------------------------------------------");
                        Console.WriteLine($"Id: {transfer_Detail.transfer_Id}");

                        List <User> userList = authService.ListUsers();
                        string myUsername = userList.Where(u => u.UserId == userId).Select(u => u.Username).FirstOrDefault();
                        if (transfer_Detail.account_From != userId)
                        {
                            string fromusername = userList.Where(u => u.UserId == transfer_Detail.account_From).Select(u => u.Username).FirstOrDefault();
                            Console.WriteLine($"From: {fromusername}");
                            Console.WriteLine($"To: Me {myUsername}");

                        }
                        else
                        {
                            string tousername = userList.Where(u => u.UserId == transfer_Detail.account_To).Select(u => u.Username).FirstOrDefault();
                            Console.WriteLine($"From: Me {myUsername}");
                            Console.WriteLine($"To: {tousername}");
                        }
                        string transferType = transfer_Detail.transfer_type_desc;
                        Console.WriteLine($"Type: {transferType}");
                        string transferStatus = transfer_Detail.transfer_status_desc;
                        Console.WriteLine($"Status: {transferStatus}");
                        Console.WriteLine($"Amount: {transfer_Detail.amount.ToString("c2")}");
                    }
                }
                else
                {
                    Console.WriteLine("Goodbye!");
                    Environment.Exit(0);
                }
            }

        }

        private static int GetTransferId(List<API_Transfer_Detail> transferList)
        {
            Console.WriteLine("Enter a transfer ID:  ");
            string transferIdStr = Console.ReadLine();
            if (int.TryParse(transferIdStr, out int transferId))
            {
                if (transferList.Any(u => u.transfer_Id == transferId))
                {
                    return transferId;
                }
                else
                {
                    Console.WriteLine("transferId not found");
                    return GetTransferId(transferList);
                }
            }
            else
            {
                Console.WriteLine("Please enter an interger for the transferId");
                return GetTransferId(transferList);

            }
        }

        private static decimal GetAmount(decimal balance, string prompt)
        {
            Console.WriteLine("Enter amount to " + prompt);
            string amountToSendStr = Console.ReadLine();
            if (decimal.TryParse(amountToSendStr, out decimal amountToSend))
            {
                if (amountToSend <= balance)
                {
                    return amountToSend;
                }
                else
                {
                    Console.WriteLine("Insufficient Funds");
                    return GetAmount(balance, prompt);
                }
            }
            else
            {
                Console.WriteLine("Invalid Entry");
                return GetAmount(balance, prompt);
            }
        }

        private static int GetUserId(List<User> userList, string prompt)
        {
            Console.WriteLine(prompt);
            string userIdToStr = Console.ReadLine();
            if (int.TryParse(userIdToStr, out int userIdTo))
            {
                if (userList.Any(u => u.UserId == userIdTo))
                {
                    return userIdTo;
                }
                else
                {
                    Console.WriteLine("UserId not found");
                    return GetUserId(userList, prompt);
                }
            }
            else
            {
                Console.WriteLine("Please enter an interger for the UserId");
                return GetUserId(userList, prompt);

            }
        }

        private static List<User> ShowUserList()
        {
            //show a list of users
            List<User> userList = authService.ListUsers();
            int currentUserId = UserService.GetUserId();
            userList = userList.Where(u => u.UserId != currentUserId).ToList();
            Console.WriteLine("ID     NAME");

            foreach (User u in userList)
            {
                    Console.WriteLine(u.UserId + "     " + u.Username);
            }
            return userList;
        }
    }
}
