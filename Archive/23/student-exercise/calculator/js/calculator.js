let display;
let previous = null;
let operator = null;
let operatorClicked = false;

/**
 * Calculates the operation and updates the display.
 */
function performOperation() {
  let result;
  console.log('display.value ' + display.value);
  console.log('previous ' + previous);
  console.log('operator ' + operator);

  const current = parseNumber(display.value);
  previous = parseNumber(previous);
  switch(operator) {
    case '+' :
      result = previous + current;
    break;
    case '-' :
        result = previous - current;
    break;
    case '*' :
        result = previous * current;
    break;
    case '/' :
        result = previous / current;
    break;
  }

  display.value = result;
  operator = null;
  document.getElementById('display').value = display.value;
}

/**
 * Parses the display value into a number (float or int).
 * @param {String} num 
 */
function parseNumber(num) {
  console.log('parseNumber ' + num + 'type ' + typeof(num));
  return num.toString().includes('.') ? parseFloat(num) : parseInt(num);
}

/**
 * Capture the previous value and the clicked operator
 * so that an operation can be performed.
 */
function clickOperator() {
  operator = event.target.value;
  previous = display.value;
  operatorClicked = true;
}

/**
 * Captures a number click and updates the display value.
 * @param {Event} event 
 */
function clickNumber(event) {
  console.log('clickNumber is triggered');
  const val = event.target.value;

  if( operatorClicked ) {
    display.value = val;
    operatorClicked = false;
  } else {
    display.value == 0 ? display.value = val : display.value += val;
  }
  document.getElementById('display').value = display.value;
}

/**
 * Resets the display value.
 */
function clear() {
  display.value = 0;
}

// add event listener for when the DOM is loaded
document.addEventListener('DOMContentLoaded', () => {

  // set the variable called display equal to the display element
  // HINT: use its id #display to get a reference to it
  display = {};
  display.value=document.getElementById('display').innerText;

  // get a reference to all of the numbers
  // loop over each of the numbers
  // add a click event listener to each number to call the function clickNumber
 let numbers = document.getElementsByClassName('number');
 for (const number of numbers) {
  number.addEventListener('click',clickNumber);
 }
  // get a reference to the decimal point button
  // add a click event listener to call the function clickNumber
  // the decimal point is part of the number so append it
  let decimal = document.getElementsByClassName('decimal')[0];
  decimal.addEventListener('click',clickNumber);

  // get a reference to the all clear button
  // add a click event listener to call the function clear  
  let allclear = document.getElementsByClassName('all-clear')[0];
  allclear.addEventListener('click',clear);

  // get a reference to all of the operators;
  // loop over each of the operators
  // add a click event listener to each operator to call the function clickOperator
  let operators = document.getElementsByClassName('operator');
  for (const operator of operators) {
    operator.addEventListener('click',clickOperator);
  }

  // add click event listener for the equal sign
  // should call the function performOperation
  let equalsign = document.getElementsByClassName('equal-sign')[0];
  equalsign.addEventListener('click',performOperation);
});

