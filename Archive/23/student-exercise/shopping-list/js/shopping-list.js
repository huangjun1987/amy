let allItemsIncomplete = true;
const pageTitle = 'My Shopping List';
const groceries = [
  { id: 1, name: 'Oatmeal', completed: false },
  { id: 2, name: 'Milk', completed: false },
  { id: 3, name: 'Banana', completed: false },
  { id: 4, name: 'Strawberries', completed: false },
  { id: 5, name: 'Lunch Meat', completed: false },
  { id: 6, name: 'Bread', completed: false },
  { id: 7, name: 'Grapes', completed: false },
  { id: 8, name: 'Steak', completed: false },
  { id: 9, name: 'Salad', completed: false },
  { id: 10, name: 'Tea', completed: false }
];

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
  const title = document.getElementById('title');
  title.innerText = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
  const ul = document.querySelector('ul');
  groceries.forEach((item) => {
    const li = document.createElement('li');
    li.innerText = item.name;
    const checkCircle = document.createElement('i');
    checkCircle.setAttribute('class', 'far fa-check-circle');
    li.appendChild(checkCircle);
li.onclick = function() {
  if(!this.classList.contains('completed')){
    this.classList.add('completed');
    this.children[0].classList.add('completed');
  }
};
li.ondblclick = function() {
  if(this.classList.contains('completed')){
    this.classList.remove('completed');
    this.children[0].classList.remove('completed');
  }
};
    ul.appendChild(li);
  });
}

/**
 * This function will be called when the button is clicked. You will need to get a reference
 * to every list item and add the class completed to each one
 */
function toggleCompleted() {
  if(allItemsIncomplete){
   markAllCompleted();
  this.innerText='Mark All Incomplete';
  allItemsIncomplete=false;
}
else{
  markAllIncomplete();
this.innerText='Mark All Complete';
allItemsIncomplete=true;
}
}
function markAllIncomplete(){
  var lis = document.querySelector('ul').getElementsByTagName("li");
  for (const li of lis) {
    if(li.classList.contains('completed')){
      li.classList.remove('completed');
      li.children[0].classList.remove('completed');
    }   
} 
}
function markAllCompleted(){
  var lis = document.querySelector('ul').getElementsByTagName("li");
  for (const li of lis) {
    if(!li.classList.contains('completed')){
      li.classList.add('completed');
      li.children[0].classList.add('completed');
    }
} 
}

  setPageTitle();
  displayGroceries();

  // Don't worry too much about what is going on here, we will cover this when we discuss events.
document.addEventListener('DOMContentLoaded', () => {
  // When the DOM Content has loaded attach a click listener to the button
  const button = document.querySelector('.btn');
  button.addEventListener('click', toggleCompleted);
});