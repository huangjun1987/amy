class ApiService {
    baseURL= "http://localhost:3000/";
    getTopics() {     
        return axios.get(this.baseURL +'topics' )       
    }
    getTopicDetail(id) {     
        return axios.get(this.baseURL +'topics/' +id )       
    }
  }
  import axios from 'axios';
  export default new ApiService();